const getResource = async (url) =>{
    const res = await fetch(url);

    if(!res.ok){
        throw new Error(`Could not fetch ${url}, status: ${res.status}`);
    }

    return await res.json();
};
     
async function countPosts(){
    const posts = await getResource('https://jsonplaceholder.typicode.com/posts')
    const users = await getResource('https://jsonplaceholder.typicode.com/users')
    
    console.log('################### ILOŚĆ NAPISANYCH POSTÓW ########################')
    users.forEach(user =>{
        user.posts = posts.filter(post => post.userId === user.id)
        console.log(`${user.name} napisał(a) ${user.posts.length} postów`)
    });
    
    return users
}

async function findUniquePosts(){
    const posts = await getResource('https://jsonplaceholder.typicode.com/posts')

    const alreadySeen = [];
    const result = []

    console.log('################### UNIKALNE POSTY ########################')
    posts.forEach(post => alreadySeen[post.title] ? result.push(post.title) : alreadySeen[post.title] = true)

    if(result.length >= 1){
        console.log(`powtażające się posty: ${[...new Set(result)]}`)
    }else{
        console.log('brak powtażających się postów')
    }

    return [...new Set(result)]
}
     
//Oblicznie odległości w km
function getDistance(lat1, lng1, lat2, lng2){
    if(typeof lat1 != "number" || typeof lng1 != "number" || typeof lat2 != "number" || typeof lng2 != "number"){
        lat1 = parseFloat(lat1)
        lng1 = parseFloat(lng1)
        lat2 = parseFloat(lat2)
        lng2 = parseFloat(lng2)
    }
    let radius = 6371 //km
    let dLat = (lat2-lat1) * Math.PI / 180
    let dLng = (lng2-lng1) * Math.PI / 180
    lat1 = lat1 * Math.PI / 180
    lat2 = lat2 * Math.PI / 180
    let val = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.sin(dLng/2) * Math.sin(dLng/2) * Math.cos(lat1) * Math.cos(lat2)    
    let ang = 2 * Math.atan2(Math.sqrt(val), Math.sqrt(1-val))
    return radius * ang
}
     
async function findClosestNeibour(){
    const users = await getResource('https://jsonplaceholder.typicode.com/users')
    console.log('################### SĄSIEDZI ########################')
    users.map(u1=>{
        u1.closestNeighbour = users.map(u2=> ({
                distance: getDistance(u1.address.geo.lat, u1.address.geo.lng, u2.address.geo.lat, u2.address.geo.lng),
                neighbourName: u2.name
        })).reduce((acc, val) => {
            if (acc.distance === 0) return val;
            if (val.distance === 0) return acc;
            return acc.distance < val.distance ? acc : val;
        });
        console.log(`dla ${u1.name} najbliższy sąsiad: ${u1.closestNeighbour.neighbourName} o odległości ${u1.closestNeighbour.distance}`)
    })
    return users;
}
     
countPosts()
findUniquePosts()
findClosestNeibour()